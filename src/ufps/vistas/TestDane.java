/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.Modelo.Departamento;
import ufps.Modelo.Persona;
import ufps.Modelo.Region;
import ufps.Negocio.Dane;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author MADARME
 */
public class TestDane {
    public static void main(String[] args) {
        Dane dane=new Dane("http://ufps30.madarme.co/dptoDane.csv");
        
        
        dane.cargarSubsidioRegion("http://ufps30.madarme.co/persistencia/subsidioregion.txt");//2
        //System.out.println(dane.getListadoRegiones());
        
        dane.cargarPersonas("http://ufps30.madarme.co/persistencia/dane_personas.txt");//3
        
        ejercicio4a(dane);
        //ejercicio4b(dane);
        //ejercicio4c(dane);
        //ejercicio4d(dane);
        
        //imprimirDane(dane);
        
    }
    
    private static void ejercicio4a(Dane dane) 
    {
        System.out.println("Cantidad de subsidios entregados: " + dane.procesarSubsidios());//4.a
    }
    
    private static void ejercicio4b(Dane dane) 
    {
        System.out.println("Lista de departamentos y la cantidad de subsidios entregados \n" + dane.getCantidadSubsidioDepartamentos().toString());
    }

    private static void ejercicio4c(Dane dane) 
    {
        System.out.println("Personas que no recibieron subsidio: \n" + dane.getPersonasNoSubsidio().toString() + "\n");
    }

    private static void ejercicio4d(Dane dane) 
    {
        Pila<Region> p = dane.getSubisidiosRegion();
        System.out.println("Pila de regiones organizadas por cantidad de subsidios otorgados:");
        while(!p.esVacia())
        {
            Region aux = p.desapilar();
            System.out.println("|" + "Region:" + aux.getNombre() + " Código:" + aux.getCodigo() + " Beneficiados:" + aux.getCantidadBeneficiarios() + "|" + "\n");
        }
    }

    private static void imprimirDane(Dane dane) {
        System.out.println(dane.toString());
    }

    

    
    
}
